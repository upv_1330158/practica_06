package course.examples.practica_06;

import android.content.Intent;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button save,load;
    EditText message, name, mail, phoneN;
    String Message, Name, Mail, PhoneN;
    int data_block = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        save = (Button)findViewById(R.id.SAVE);
        load = (Button)findViewById(R.id.LOAD);
        name = (EditText)findViewById(R.id.name);
        mail = (EditText)findViewById(R.id.mail);
        phoneN = (EditText)findViewById(R.id.phoneN);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Name = name.getText().toString();
                Mail = mail.getText().toString();
                PhoneN = phoneN.getText().toString();
                try {

                    File sdCard = Environment.getExternalStorageDirectory();
                    File directory = new File (sdCard.getAbsolutePath() + "/MyFiles");
                    directory.mkdirs();
                    File file = new File(directory, "textfile.txt");
                    FileOutputStream fOut = new FileOutputStream(file);

                    OutputStreamWriter osw = new OutputStreamWriter(fOut);

                    try{
                        osw.write("Contact Name: "+ Name);
                        osw.append("\r\n");
                        osw.write("Mail: "+Mail);
                        osw.append("\r\n");
                        osw.write("Phone Number: "+PhoneN);
                        //osw.append(Message);
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(),"Dato guardado", Toast.LENGTH_LONG).show();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //---SD Storage---
                    File sdCard = Environment.getExternalStorageDirectory();
                    File directory = new File (sdCard.getAbsolutePath() + "/MyFiles");
                    File file = new File(directory, "textfile.txt");
                    FileInputStream fIn = new FileInputStream(file);
                    InputStreamReader isr = new InputStreamReader(fIn);

                    char[] inputBuffer = new char[data_block];
                    String final_data = "";
                    int charRead;
                    while ((charRead = isr.read(inputBuffer))>0)
                    {
                        //---convert the chars to a String---
                        String readString = String.copyValueOf(inputBuffer, 0,
                                charRead);
                        final_data += readString;
                        inputBuffer = new char[data_block];
                    }
                    //---set the EditText to the text that has been
                    // read---
                    Toast.makeText(getBaseContext(),final_data,Toast.LENGTH_LONG).show();
                    //Toast.makeText(getBaseContext(), "File loaded successfully!", Toast.LENGTH_SHORT).show();
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }

        });
    }


   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_search:
                //openSearch();
                startActivity(new Intent(Settings.ACTION_SEARCH_SETTINGS));
                return true;
            case R.id.action_settings:
                //openSettings();
                startActivity(new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
